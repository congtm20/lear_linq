﻿using LinQ;
using System.Linq;


var brands = new List<Brand>() {
    new Brand{ID = 1, Name = "Công ty AAA"},
    new Brand{ID = 2, Name = "Công ty BBB"},
    new Brand{ID = 4, Name = "Công ty CCC"},
};

var products = new List<Product>()
{
    new Product(1, "Bàn trà",    400, new string[] {"Xám", "Xanh"},         2),
    new Product(2, "Tranh treo", 400, new string[] {"Vàng", "Xanh"},        1),
    new Product(3, "Đèn trùm",   500, new string[] {"Trắng"},               3),
    new Product(4, "Bàn học",    200, new string[] {"Trắng", "Xanh"},       1),
    new Product(5, "Túi da",     300, new string[] {"Đỏ", "Đen", "Vàng"},   2),
    new Product(6, "Giường ngủ", 500, new string[] {"Trắng"},               2),
    new Product(7, "Tủ áo",      600, new string[] {"Trắng"},               3),
};


var qr = from p in products
         group p by p.Price into prod
         orderby prod.Key
         let sl = "Số lượng là: " + prod.Count()
         select new
         {
             Gia = prod.Key,
             SanPham = prod,
             Soluong = sl
         };
qr.ToList().ForEach(x =>
{
    Console.WriteLine(x.Gia);
    Console.WriteLine(x.Soluong);
    x.SanPham.ToList().ForEach(p => Console.WriteLine(p));
});


/*Cấu trúc
 * 1) Xác định nguồn dữ liệu: from ten in Inumerables
 *  ...join, Where, OrderBy, let tenBien...
 * 2) Lấy dữ liệu: Select, GroupBy,...
 *  var result = from a in products
                select a.Name;
    result.ToList().ForEach(items => Console.WriteLine(items));
 * 
 */
/*  Ví dụ thực hành
// In ra tên sản phẩm, Tên thương hiệu, Có giá (300-400), sắp xếp giảm dần
    products.Where(p => p.Price >= 300 && p.Price <= 400)
        .OrderByDescending(p => p.Price)
        .Join(brands, p => p.Brand, b => b.ID, (sp, th) =>
        {
            return new
            {
                TenSp = sp.Name,
                TenTh = th.Name,
                GiaSp = sp.Price
            };

        })
        .ToList()
        .ForEach(p=>Console.WriteLine($"{p.TenSp,15}{p.TenTh,15}{p.GiaSp,10}"));
 */
/*  Count đếm tất cả các phần tử hoặc số phần tử thỏa mãn đk
 *  var p = products.Count(); // 7
    var p = products.Count(p => p.Price >= 300); // 6
 */
/*  All kiểm tra tất cả phần tử có thỏa mãn đk hay không. Thỏa mãn thì trả về true không thì false.
 *  var p = products.All(p => p.Price >= 200); // true
    var p = products.All(p => p.Price >= 300); // false
 */
/*  Any kiểm tra xem có 1 phần tử nào thỏa mãn đk hay không. Thỏa mãn thì trả về true không thì false.
 *  var p = products.Any(p => p.ID == 1); //true
    var p = products.Any(p => p.ID == 10); //false
 */
/*  SingleOrDefault kiểm tra các phần tử thỏa mãn 1 đk nào đó. Chỉ trả về 1 kết quả nếu không tìm thấy trả về null
 *  nếu nhiều hơn 1 kết quả sẽ xảy ra lỗi.
 *  var p = products.SingleOrDefault(p => p.Price <= 600);
 
 */
/*  Single kiểm tra các phần tử thỏa mãn 1 đk nào đó. Chỉ trả về 1 kết quả nếu nhiều hơn hoặc k tìm thấy 
 *  sẽ xảy ra lỗi.
 *  var p = products.Single(p => p.Price == 600);
 
 */
/* Distinct: Xóa các giá trị trùng nhau chỉ giữu lại 1 giá trị duy nhất.
 *  products.SelectMany(p=>p.Colors).Distinct().ToList().ForEach(p=>Console.WriteLine(p));
 
 */
/* GroupBy: Trả về một tập hợp. Mỗi phần tử là một nhóm theo một dữ liệu nào đó.
 *  var result = products.GroupBy(p => p.Price);
    foreach(var group in result)
    {
        Console.WriteLine(group.Key);
        foreach(var p in group)
        {
            Console.WriteLine(p);
        }
    }
 */
/* Reverse Đảo ngược thứ tự
 *  int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, }; 
    numbers.Reverse().ToList().ForEach(n=>Console.WriteLine(n));
 
 */
/* OrderByDescending: Sắp xếp theo giảm dần.
 * products.OrderByDescending(p=>p.Price).ToList().ForEach(p=>Console.WriteLine(p));
 
 */
/* OrderBy: Sắp xếp theo tăng dần.
 * products.OrderBy(p=>p.Price).ToList().ForEach(p=>Console.WriteLine(p));
 
 */
/* Skip: Bỏ qua n phân tử đầu tiên lấy các giá trị tiếp theo
 * products.Take(n).ToList().ForEach(p => Console.WriteLine(p));
 
 */
/*Take: Lấy n phân tử đầu tiên
 * products.Take(n).ToList().ForEach(p => Console.WriteLine(p));
 
 */
/* GroupJoin
 * var result = brands.GroupJoin(products, b => b.ID, p => p.Brand,
    (brand, prod) =>
    {
        return new
        {
            Thuonghieu = brand.Name,
            Cacsanpham = prod
        };
    });
    foreach (var gr in result)
    {
        Console.WriteLine(gr.Thuonghieu);
        foreach(var p in gr.Cacsanpham)
            {
                Console.WriteLine(p);
            }
    }
 */
/* Join
 * var result = products.Join(brands, p => p.Brand, b => b.ID, (p, b) =>
      {
          return new
          {
              Ten = p.Name,
              ThuongHieu = b.Name
          };
      });
 */
/* SelectMany
 * var result = products.SelectMany(
    (p) =>
    {
    return p.Colors;
    });
 */
/* Where
 var result = products.Where(
(p) =>
{
    return p.Price >= 200 && p.Name.Contains("c");
});
 */
/* Select
 * var result = products.Select(
     p => { 
     return p.Name; 
    });
 */


